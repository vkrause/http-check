#!/usr/bin/env python3

# Copyright 2019 Sandro Knauß <sknauss@kde.org>
# Copyright 2019 Volker Krause <vkrause@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import fnmatch
import glob
import os
import re
import sys

class HTTPChecker:
    def __init__(self, blacklist):
        self.blacklist = blacklist
        self.reBlacklist = list(map(lambda r:re.compile(r,re.I), blacklist))
        self.matcher = re.compile(r'http://\S', re.I)

    def checkFile(self, fpath):
        findings = []
        with open(fpath, 'r') as f:
            if fpath.startswith('./'):
                fpath = fpath[2:]
            linenumber = 0
            for line in f:
                linenumber += 1
                m = self.matcher.search(line)
                if m:
                    if m.group(0)[-1] == ".":
                        continue
                    if not any(map(lambda r: r.search(line), self.reBlacklist)):
                        print("{}:{}:\t{}".format(fpath,linenumber,line.strip()))
                        findings.append((linenumber, line))
        return findings

def loadBlacklist(path, ignoreAuto=False):
    blacklist = []
    try:
        with open(path) as f:
            comment = ""
            for line in f:
                m = re.match(r"^\s*#\s*(.*)", line)
                if m:
                    comment = m.group(1).strip()
                    continue

                if ignoreAuto and comment.startswith("Auto:"):
                    comment = ""
                    continue

                if line.strip():
                    blacklist.append(line.strip())
                comment = ""
    except FileNotFoundError:
        pass
    return blacklist

def loadBlacklist(path, ignoreAuto=False):
    blacklist = []
    try:
        with open(path) as f:
            comment = ""
            for line in f:
                m = re.match(r"^\s*#\s*(.*)", line)
                if m:
                    comment = m.group(1).strip()
                    continue

                if ignoreAuto and comment.startswith("Auto:"):
                    comment = ""
                    continue

                if line.strip():
                    blacklist.append(line.strip())
                comment = ""
    except FileNotFoundError:
        pass
    return blacklist

def getBlacklist(path, ignoreAuto=False):
    blacklist = []
    for fname in glob.glob(os.path.join(os.path.dirname(__file__), "*.htignore")):
        if ignoreAuto and fname.endswith('reduce-warning.htignore'):
            continue
        blacklist += loadBlacklist(fname, ignoreAuto)
    return blacklist + loadBlacklist(path, ignoreAuto)

class GitIgnore:
    def __init__(self, path):
        self.basepath = path
        self.patterns = []
        try:
            with open(os.path.join(path, '.gitignore')) as f:
                for line in f:
                    self.patterns.append(line.strip())
        except FileNotFoundError:
            pass

    def match(self, path):
        for pattern in self.patterns:
            if fnmatch.fnmatch(os.path.relpath(path, self.basepath), pattern):
                return True
        return False

def main(path):
    blacklist = getBlacklist(os.path.join(path, '.htignore'))
    checker = HTTPChecker(blacklist)
    gitIgnore = GitIgnore(path)
    gitIgnore.patterns.append('.htignore')
    gitIgnore.patterns.append('.gitignore')
    findings = {}
    for dirpath, dirnames, filenames in os.walk(path):
        parts = dirpath.split("/")
        if any(map(lambda p: p in parts, ['.git', 'tests', 'autotests', '3rdparty'])):
            continue
        for fname in filenames:
            fpath = os.path.join(dirpath, fname)
            if gitIgnore.match(fpath):
                continue
            try:
                issues = checker.checkFile(fpath)
                if issues:
                    findings[fpath] = issues
            except UnicodeDecodeError:
                pass
    if findings:
        sys.exit(1)
    else:
        sys.exit(0)

if __name__ == "__main__":
    path = "."
    if len(sys.argv) > 1:
        path = sys.argv[1]
    urls = main(path)
