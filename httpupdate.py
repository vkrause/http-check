#!/usr/bin/env python3

# Copyright 2019 Sandro Knauß <sknauss@kde.org>
# Copyright 2019 Volker Krause <vkrause@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import collections
import dns.resolver
import glob
import fnmatch
import multiprocessing
import os
import re
import requests
import socket
import ssl
import sys
import urllib.parse
import urllib.request

RESOLVER = dns.resolver.Resolver()
RESOLVER.nameservers=["1.1.1.1", "8.8.8.8"]

class HTTPChecker:
    def __init__(self, blacklist):
        self.blacklist = blacklist
        self.reBlacklist = list(map(lambda r:re.compile(r,re.I), blacklist))
        self.updater = re.compile(r'(https?://[^. \t\n"\'?<>*#()\\{}][^ \t\n"\'?<>*#()\\{},]+)', re.I)

    def urls(self, fpath):
        urls = set()
        with open(fpath, 'r') as f:
            linenumber = 0
            for line in f:
                linenumber += 1
                if any(map(lambda r: r.search(line), self.reBlacklist)):
                    continue
                for link in self.updater.findall(line):
                    urls.add(link)
        return urls

class Url:
    def __init__(self, url):
        self.url = url
        m = re.match(r"^(https?)://([^/]+)((/.*)$|$)", self.url, re.I)
        self.protocol = m.group(1)
        self.domain = m.group(2)
        self.path = m.group(3)
        self.resolver = RESOLVER
        self.checkState = None

    def checkDns(self):
        try:
            self.resolver.query(self.domain, "A")
            return True
        except (dns.resolver.NoAnswer, dns.resolver.NXDOMAIN, dns.resolver.NoNameservers):
            pass

        try:
            self.resolver.query(self.domain, "AAAA")
            return True
        except (dns.resolver.NoAnswer, dns.resolver.NXDOMAIN, dns.resolver.NoNameservers):
            return False

    def checkUrl(self):
        try:
            r = requests.get(self.url, timeout=5)
            if r.status_code in (200, 400, 401, 403, 405, 406):
                return True
        except (ssl.SSLError):
            return False
        except UnicodeEncodeError:
            pass
        except (requests.ConnectionError, requests.Timeout):
            pass
        try:
            u = urllib.request.urlopen(self.url, timeout=5)
            if u.status == 200:
                return True
        except (ssl.SSLError):
            return False
        except UnicodeEncodeError:
            return True
        except (urllib.request.HTTPError, urllib.request.URLError, socket.timeout):
            return False
        return False

    @property
    def secureUrl(self):
        return "https://{}{}".format(self.domain, self.path)

    def checkSecureUrl(self):
        try:
            r = requests.get(self.secureUrl, timeout=5)
            if r.status_code in (200, 400, 401, 403, 405, 406):
                return True
        except (ssl.SSLError):
            return False
        except UnicodeEncodeError:
            pass
        except (requests.ConnectionError, requests.Timeout):
            pass
        try:
            u = urllib.request.urlopen(self.secureUrl, timeout=5)
            if u.status == 200:
                return True
        except (ssl.SSLError):
            return False
        except UnicodeEncodeError:
            return True
        except (urllib.request.HTTPError, urllib.request.URLError, socket.timeout):
            return False
        return False

    def check(self):
        if not self.checkDns():
            self.checkState = "checkDns"
            return self.checkState

        if not self.checkUrl():
            self.checkState = "checkUrl"
            return self.checkState

        if self.protocol == "http" and not self.checkSecureUrl():
            self.checkState = "checkSecureUrl"
            return self.checkState

        self.checkState = True
        return self.checkState

def check(url):
    url.check()
    return url

def loadBlacklist(path, ignoreAuto=False):
    blacklist = []
    try:
        with open(path) as f:
            comment = ""
            for line in f:
                m = re.match(r"^\s*#\s*(.*)", line)
                if m:
                    comment = m.group(1).strip()
                    continue

                if ignoreAuto and comment.startswith("Auto:"):
                    comment = ""
                    continue

                if line.strip():
                    blacklist.append(line.strip())
                comment = ""
    except FileNotFoundError:
        pass
    return blacklist

def getBlacklist(path, ignoreAuto=False):
    blacklist = []
    for fname in glob.glob(os.path.join(os.path.dirname(__file__), "*.htignore")):
        if ignoreAuto and fname.endswith('reduce-warning.htignore'):
            continue
        blacklist += loadBlacklist(fname, ignoreAuto)
    return blacklist + loadBlacklist(path, ignoreAuto)

class GitIgnore:
    def __init__(self, path):
        self.basepath = path
        self.patterns = []
        try:
            with open(os.path.join(path, '.gitignore')) as f:
                for line in f:
                    self.patterns.append(line.strip())
        except FileNotFoundError:
            pass

    def match(self, path):
        for pattern in self.patterns:
            if fnmatch.fnmatch(os.path.relpath(path, self.basepath), pattern):
                return True
        return False

def main(path):
    blacklist = getBlacklist(os.path.join(path, '.htignore'), ignoreAuto=True)
    checker = HTTPChecker(blacklist)
    gitIgnore = GitIgnore(path)
    gitIgnore.patterns.append('.htignore')
    gitIgnore.patterns.append('.gitignore')
    urls = collections.defaultdict(set)
    for dirpath, dirnames, filenames in os.walk(path):
        parts = dirpath.split("/")
        if any(map(lambda p: p in parts, ['.git', 'tests', 'autotests', '3rdparty'])):
            continue
        for fname in filenames:
            fpath = os.path.join(dirpath, fname)
            if gitIgnore.match(fpath):
                continue
            try:
                u = checker.urls(fpath)
                for url in u:
                    urls[url].add(fpath)
            except UnicodeDecodeError:
                pass

    manual_overwrites = []
    try:
        with open(os.path.join(path, '.htignore')) as f:
            comment = None
            for line in f:
                m = re.match(r"^\s*#\s*(.*)", line)
                if m:
                    comment = m.group(1).strip()
                    continue

                if comment and comment.startswith("Auto:"):
                    comment = None
                    continue

                if line.strip():
                    manual_overwrites.append((line.strip(), comment))
                comment = None

    except FileNotFoundError:
        pass

    auto_overwrites = []
    with multiprocessing.Pool(processes=40) as pool:
        for url in pool.imap_unordered(check, [Url(u) for u in urls.keys()]):
            updateResult = url.checkState
            if updateResult == True and url.protocol == "http":
                for fpath in urls[url.url]:
                    print(f"updating {fpath}")
                    with open(fpath, 'r') as f:
                        content = f.read()
                    with open(fpath, 'w') as f:
                        f.write(re.sub(url.url.replace(".","\."),url.secureUrl, content, flags=re.I))
            elif updateResult != True:
                if updateResult == "checkDns":
                    description = "No DNS response"
                if updateResult == "checkUrl":
                    description = "URL seems dead"
                if updateResult == "checkSecureUrl":
                    description = "No https alternative"
                l = "\n\t".join(urls[url.url])
                print(f"'{description}' '{url.url}' found in:\n\t{l}")
                if url.protocol == "http":
                        auto_overwrites.append((url.url.replace(".","\."), f"Auto: {description}"))

    if not manual_overwrites and not auto_overwrites:
        return

    with open(os.path.join(path, '.htignore'), 'w') as ow:
        if manual_overwrites:
            ow.write("\n".join([formatOverwrite(i) for i in manual_overwrites]))
            ow.write("\n")

        if auto_overwrites:
            ow.write("\n".join([formatOverwrite(i) for i in sorted(auto_overwrites)]))
            ow.write("\n")

def formatOverwrite(entry):
    if entry[1]:
        return f"# {entry[1]}\n{entry[0]}"
    else:
        return entry[0]


if __name__ == "__main__":
    path = "."
    if len(sys.argv) > 1:
        path = sys.argv[1]
    urls = main(path)
